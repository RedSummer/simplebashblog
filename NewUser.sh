#!/bin/bash
read -p "User: " Username
Userpasswd=$( pwgen 14 1 )

mkdir /var/www/html/$Username && mkdir /var/www/html/$Username/public && echo "Direcotrios publicos creados"
useradd -d /var/www/html/$Username/public -G sftponly,mail -s /bin/false $Username && echo "Usuario creado"
touch /var/www/html/$Username/public/index.html && echo "Works!" > /var/www/html/$Username/public/index.html && echo "Fichero de prueba creado"
echo $Username:$Userpasswd | chpasswd && echo "Contrasena actualizada: "$Userpasswd
ipfsHash=$( ipfs add -r /var/www/html/$Username/public | grep "public" | tail -n1 | awk  '{print $2}' )
ipfs key gen --type=rsa --size=2048 $Username && echo "Clave IPNS creada"

echo "Subiendo a IPFS y creando IPNS..."
touch $Username"_NewUser.txt"
echo "Usuario:"$Username >> $Username"_NewUser.txt"
echo "Contrasena:"$Userpasswd  >> $Username"_NewUser.txt"
echo "IPNS:"${Ipnshash%?} >> $Username"_NewUser.txt"
ipfs name publish --key=$Username $ipfsHash | awk '{print $3}' >> $Username"_NewUser.txt" &

pid=$!
# If this script is killed, kill the `cp'.
trap "kill $pid 2> /dev/null" EXIT
# While copy is running...
while kill -0 $pid 2> /dev/null; do
    # Do stuff
	chars="/-\|"
	 while true; do
  		for (( i=0; i<${#chars}; i++ )); do
    			sleep 0.5
    			echo -en " ${          chars:$i:1}" "\r"
 		done
	done
sleep 1 done
# Disable the trap on a normal exit.
trap - EXIT
